<?php

    date_default_timezone_set('Asia/Dhaka');
    include 'dbh.inc.php';
    include 'comment.inc.php';
    session_start();




?>





<!DOCTYPE html>
<html>
<head>
    <title>Day 6 Bootstrap Practice</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

</head>

<body>




<header>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Brand</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">Link</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Link</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</header>

<!-- ###############        login logout system      ######## -->



<form method='POST' acttion='<?php getLogin($conn)?>'>
    <input type='text' name='uid'>
    <input type='password' name='pwd'>
    <button type='submit' name='loginSubmit'>Login</button>
</form>




<form method='POST' action='<?php userLogout()?>'>
    <button type='submit' name='logoutSubmit'>Logout</button>
</form>
<?php
if(isset($_SESSION['id'])){

    echo "you are logged in";
}
else{
    echo "you are not logged in";
}
?>



<br><br>



<!-- #################       COMMENT      #################-->
<?php

if(isset($_SESSION['id'])){ ?>
    <form method='POST' action='<?php setComments($conn)?>'>

        <input type='hidden' name='uid' value='Anonymous'>
        <input type='hidden' name='date' value='<?php date('Y-m-d H:i:s')?>'>
        <textarea name='message'></textarea><br>

        <button type='submit' name='commentSubmit'>Comment</button><br>

    </form>
    <?php
}

else{
    echo "you need to be logged in to comment!";
}
getCommetns($conn);
?>


<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>